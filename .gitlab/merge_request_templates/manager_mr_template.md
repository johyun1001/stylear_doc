# Style AR Manager!

새로운 기능을 만들자!

issue를 이용하여 요청하시면 됩니다.

# 요청방식
!. 제목에 말머리를 달아주셔야합니다!
 - 매니저 전체에 적용되거나 모든 제품에 사용될 기능은 [All] 말머리 하나만 달아 주시면 됩니다.
 - 제품, 메뉴별로 나뉘어지는 기능은 아래 말머리 규칙처럼 작성해 주시면 됩니다.
![말머리규칙](./images/Guide01.png)

그 외 요청사항은 자유롭게 작성하시고 wonmo.yeo와 의논해주시면 됩니다!

## issue labels

- `new-feature`

  - 새로운 기능 추가 요청

- `develop-feature`

  - 기존 기능 개선 요청

#### Label for work

- `sprint`

  - 작업 리스트에 올려져 있음

- `dev`

  - 현재 dev에 문제 해결되어있음. 라이브 대기중

- `continue`

  - 현재 작업중인 리스트

- `complete`
  - dev에 반영되진 않았지만 작업은 완료한 상태.
